import {
  EventStoreDBClient,
  jsonEvent,
  streamNameFilter,
} from '@eventstore/db-client';
import { v4 } from 'uuid';

const delay = (milli: number) =>
  new Promise((resolve) => setTimeout(() => resolve(undefined), milli));

describe('TestEventStore', () => {
  let eventStore: EventStoreDBClient;
  beforeAll(async () => {
    eventStore = new EventStoreDBClient(
      {
        endpoint: 'localhost:2113',
      },
      {
        insecure: true,
      },
    );
  });

  afterAll(async () => {
    await eventStore.dispose();
  });

  test('Should write a message into stream', async () => {
    const entityId = v4();
    const eventId = v4();
    const streamId = 'test_stream-' + entityId;
    await eventStore.appendToStream(
      streamId,
      jsonEvent({
        type: 'TestEvent',
        id: eventId,
        data: {
          eventSourcingWorking: true,
        },
      }),
    );
    const stream = eventStore.readStream(streamId);
    for await (const message of stream) {
      expect(message.event?.id).toEqual(eventId);
      expect(message.event?.type).toEqual('TestEvent');
      expect(message.event?.data).toMatchObject({
        eventSourcingWorking: true,
      });
    }
  });

  test('Should subscribe to stream messages', async () => {
    const entityId = v4();
    const streamId = 'outra-' + entityId;
    const readedEvents: any = [];
    const writeEvents = [
      {
        type: 'Evento01',
        id: v4(),
        data: {
          evento01: true,
        },
      },
      {
        type: 'Evento02',
        id: v4(),
        data: {
          evento02: true,
        },
      },
    ];

    eventStore.subscribeToStream(streamId).on('data', (message) => {
      console.log('Recebeu evento');
      readedEvents.push(message.event?.data);
    });

    for (const event of writeEvents) {
      await eventStore.appendToStream(streamId, jsonEvent(event));
    }
    await delay(1000);
    readedEvents.forEach((event, index) => {
      expect(event).toMatchObject(writeEvents[index].data);
    });
  });

  test('Should receive events only of entity stream', async () => {
    const pedro = v4();
    const pedroStreamId = 'usuarios-' + pedro;
    const pedroEvents: any = [
      {
        type: 'Registered',
        id: v4(),
        data: {
          name: 'Pedro',
          id: pedro,
        },
      },
    ];

    const joao = v4();
    const joaoStreamId = 'usuarios-' + joao;
    const joaoEvents: any = [
      {
        type: 'Registered',
        id: v4(),
        data: {
          name: 'Joao',
          id: joao,
        },
      },
      {
        type: 'NameChanged',
        id: v4(),
        data: {
          newName: 'Marcela',
          id: joao,
        },
      },
    ];

    const readedEvents: any[] = [];
    eventStore.subscribeToStream(joaoStreamId).on('data', (message) => {
      console.log('Recebeu evento');
      readedEvents.push(message.event?.data);
    });

    for (const event of joaoEvents) {
      await eventStore.appendToStream(joaoStreamId, jsonEvent(event));
    }

    for (const event of pedroEvents) {
      await eventStore.appendToStream(pedroStreamId, jsonEvent(event));
    }

    await delay(1000);
    expect(readedEvents.length).toEqual(2);
    readedEvents.forEach((event, index) => {
      expect(event).toMatchObject(joaoEvents[index].data);
    });
  });

  test('Should listen to category events', async () => {
    const randomStreamCategory = v4();
    const pedro = v4();
    const pedroStreamId = `${randomStreamCategory}-${pedro}`;
    const pedroEvents: any = [
      {
        type: 'Registered',
        id: v4(),
        data: {
          name: 'Pedro',
          id: pedro,
        },
      },
    ];

    const joao = v4();
    const joaoStreamId = `${randomStreamCategory}-${joao}`;
    const joaoEvents: any = [
      {
        type: 'Registered',
        id: v4(),
        data: {
          name: 'Joao',
          id: joao,
        },
      },
      {
        type: 'NameChanged',
        id: v4(),
        data: {
          oldName: 'Joao',
          newName: 'Marcela',
          id: joao,
        },
      },
    ];

    const allSystemUsers: string[] = [];
    eventStore
      .subscribeToAll({
        filter: streamNameFilter({
          prefixes: [`${randomStreamCategory}-`],
        }),
      })
      .on('data', (message) => {
        const type = message.event!.type;
        const data = message.event!.data as Record<string, any>;
        if (type === 'Registered') {
          allSystemUsers.push(data.name);
        }
        if (type === 'NameChanged') {
          allSystemUsers.splice(
            allSystemUsers.findIndex((e) => e === data.oldName),
            1,
          );
          allSystemUsers.push(data.newName);
        }
      });

    for (const event of joaoEvents) {
      await eventStore.appendToStream(joaoStreamId, jsonEvent(event));
    }

    for (const event of pedroEvents) {
      await eventStore.appendToStream(pedroStreamId, jsonEvent(event));
    }
    await delay(1000);
    console.log(allSystemUsers);
    expect(allSystemUsers[0]).toEqual('Marcela');
    expect(allSystemUsers[1]).toEqual('Pedro');
  });
});
